<?php
/**
 * Clase para consultas a la base de datos
 */
 class myModel
 {
    # variables de conexion
    private $host = 'localhost';
    private $user = 'root';
    private $pass = '';
    private $dbname = 'mybd';

    # variables a ocupar
    private $conn = false;
    private $result = [];
    private $errors = "";
    private $myQuery = "";
    private $numResult = 0;
    private $myconn = null;

    public function conectar()
    {
        if (!$this->conn) {
            $this->myconn = @mysqli_connect($this->host, $this->user, $this->pass, $this->dbname);
            if ($this->myconn) {
                $this->conn = true;
            } else {
                $this->errors = "Error de conexion [".mysqli_connect_error()."]";
                $this->conn = false;
            }
        }
    }

    public function desconectar()
    {
        if ($this->conn) {
            if (@mysqli_close($this->myconn)) {
                $this->conn = false;
            }
        }
    }

    public function sql($sql)
    {
        $this->conectar();
        $query = mysqli_query($this->myconn, $sql);
        $this->myQuery = $sql;
        if ($query) {
            $this->numResult = $query->num_rows;
            if ($this->numResult > 0) {
                for ($i=0; $i < $this->numResult; $i++) { 
                    $r = mysqli_fetch_array($query); // resultado
                    $key = array_keys($r); // campos
                    for ($x=0; $x < count($key); $x++) { 
                        if (!is_int($key[$x])) {
                            $this->result[$i][$key[$x]] = $r[$key[$x]];
                        }
                    }
                }
            } else {
                $this->errors = "No se encontraron resultados en la consulta";
            }

        } else {
            $this->errors = "Error en la consulta [".mysqli_error($this->myconn)."]";
        }
        return $this->getResults();
    }

    public function find($table, $id, $where = null)
    {
        $this->conectar();
        if ($this->tableExists($table)) {
            $primary = $this->getPrimaryKey($table);
            if (!is_null($primary)) {
                $consulta = "SELECT * FROM $table WHERE $primary = '$id'";
                if (!is_null($where)) {
                    if (is_array($where)) {
                        foreach ($where as $key => $value) {
                            $consulta .= " AND $key = '$value'";
                        }
                    }
                    if (is_string($where)) {
                        $consulta .= " AND $where";
                    }
                }
                $consulta .= " LIMIT 1";
                $this->myQuery = $consulta;
                $query = mysqli_query($this->myconn, $consulta);
                if ($query) {
                    $this->numResult = $query->num_rows;
                    if ($this->numResult > 0) {
                        for ($i=0; $i < $this->numResult; $i++) {
                            $r = mysqli_fetch_array($query); // resultado
                            $key = array_keys($r); // campos
                            for ($x=0; $x < count($key); $x++) { 
                                if (!is_int($key[$x])) {
                                    $this->result[$key[$x]] = $r[$key[$x]];
                                }
                            }
                        }
                    } else {
                        $this->errors = "No se encontraron registros";
                    }
                } else {
                    $this->errors = "Error al consultar a la BD.[".mysqli_error($this->myconn)."]";
                }
            }
        }
        return $this->getResults();
    }

    public function seleccionar($table, $campos, $where = null, $limit = null, $join = null)
    {
        $this->conectar();
        if ($this->tableExists($table)) {
            $consulta = "SELECT $campos FROM $table";
            if (!is_null($join)) {
                if (is_string($join)) {
                    $consulta .= " $join";
                }
            }
            $w = "";
            if (!is_null($where)) {
                if (is_array($where)) {
                    foreach ($where as $key => $value) {
                        if ($w == "") {
                            $w = "$key = $value";
                        } else {
                            $w .= " AND $key = $value";
                        }
                        
                    }
                }
                if (is_string($where)) {
                    $w = $where;
                }
                $consulta .= " WHERE $w";
            }
            if (!is_null($limit)) {
                $consulta .= " limit $limit";
            }
            $this->myQuery = $consulta;
            $query = mysqli_query($this->myconn, $consulta);
            if ($query) {
                if ($query->num_rows > 0) {
                    $this->numResult = $query->num_rows;
                    for ($i=0; $i < $this->numResult; $i++) { 
                        $r = mysqli_fetch_array($query); // resultado
                        $key = array_keys($r); // campos
                        for ($x=0; $x < count($key); $x++) { 
                            if (!is_int($key[$x])) {
                                if (is_null($limit) || $limit > 1) {
                                    $this->result[$i][$key[$x]] = $r[$key[$x]];
                                } else {
                                    $this->result[$key[$x]] = $r[$key[$x]];
                                }
                            }
                        }
                    }
                } else {
                    $this->errors = "No se encontraron registros en la consulta";
                }
            } else {
                $this->errors = "Error al consultar la BD.[".mysqli_error($this->myconn)."]";
            }
        }
        return $this->getResults();
    }

    public function tableExists($table)
    {
        $tableInDB = @mysqli_query($this->myconn, "SHOW TABLES FROM $this->dbname LIKE '$table'");
        if ($tableInDB) {
            if ($tableInDB->num_rows == 1) {
                return true;
            } else {
                array_push($errors, "La tabla $table no existe en la BD");
                return false;
            }
        }
        array_push($errors, "Error al conectar la BD.[".mysqli_error($this->myconn)."]");
        return false;
    }

    public function getPrimaryKey($table)
    {
        $column = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = '$this->dbname' AND TABLE_NAME = '$table' AND COLUMN_KEY = 'PRI'";
        $query = @mysqli_query($this->myconn, $column);
        if ($query) {
            if ($query->num_rows > 0) {
                $row  = $query->fetch_object();
                return $row->COLUMN_NAME;
            }
            $this->errors = "No se encontro datos con la tabla mencionada.";
            return null;
        }
        $this->errors = "Error al consultar la BD.[".mysqli_error($this->myconn)."]";
        return null;
    }

    public function getResults()
    {
        $val = $this->result;
        $er = $this->errors;
        $this->result = [];
        $this->desconectar();
        if ($er != '') {
            return $er;
        }
        return $val;
    }

    public function getSql()
    {
        $val = $this->myQuery;
        $this->myQuery = "";
        return $val;
    }

    public function numRows()
    {
        $val = $this->numResult;
        $this->numResult = 0;
        return $val;
    }
 }
?>
